package main

import (
	"context"

	"gitlab.com/rwxrob/cmdtab"
	"gitlab.com/rwxrob/wsutil"
)

func init() {
	x := cmdtab.New("append")
	x.Usage = `<url> <file>`
	x.Summary = `start a websocket and append output to file`
	x.Method = func(args []string) error {
		if len(args) != 2 {
			return x.UsageError()
		}
		ctx, cancel := context.WithCancel(context.Background())
		go wsutil.Append(ctx, args[0], args[1])
		cmdtab.WaitForInterrupt(cancel)
		return nil
	}
}
