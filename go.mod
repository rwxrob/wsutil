module gitlab.com/rwxrob/wsutil

go 1.15

replace gitlab.com/rwxrob/cmdtab => ../cmdtab

require (
	github.com/gorilla/websocket v1.4.2
	gitlab.com/rwxrob/cmdtab v0.0.0-00010101000000-000000000000
)
