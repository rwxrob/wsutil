# Go Websockets Helper Utility

![WIP](https://img.shields.io/badge/status-wip-red.svg)
[![GoDoc](https://godoc.org/gitlab.com/rwxrob/wsutil?status.svg)](https://godoc.org/gitlab.com/rwxrob/wsutil)
[![License](https://img.shields.io/badge/license-Apache-brightgreen.svg)](LICENSE)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/rwxrob/wsutil)](https://goreportcard.com/report/gitlab.com/rwxrob/wsutil)
[![Coverage](https://gocover.io/_badge/gitlab.com/rwxrob/wsutil)](https://gocover.io/gitlab.com/rwxrob/wsutil)

```
wsutil help
wsutil append <wsurl> <file>
```

The `wsutil` package contains helper functions that use
`github.com/gorilla/websockets` for dealing with the most common use
cases for websockets as well as help when prototyping websockets
applications.
