package wsutil

import (
	"context"
	"log"
	"os"

	"github.com/gorilla/websocket"
)

// Append connects to the specified URL and appends everything received
// to the specified file until canceled with the given context.Status
// and errors are logged rather than returned. The URL will never be
// logged or printed allowing it to contain sensitive token data.
func Append(ctx context.Context, url, path string) {

	// open the file for appending
	file, err := os.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	defer file.Close()
	if err != nil {
		log.Println(err)
		return
	}
	log.Printf("appending events to file: %v\n", path)

	// dial up the service
	conn, _, err := websocket.DefaultDialer.Dial(url, nil)
	if err != nil {
		log.Println(err)
		return
	}
	defer conn.Close()
	log.Println("connected to websocket server")

	// loop, read message and log it until shut down
	first := true
	for {
		select {
		case <-ctx.Done():
			log.Printf("shutting down websocket server")
			return
		default:
			_, msg, err := conn.ReadMessage() // blocks waitin
			if first {
				log.Println("received first websocket message")
				first = false
			}
			_, err = file.WriteString(string(msg) + "\n")
			if err != nil {
				log.Println(err)
				return
			}
		}
	}
}
